import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import AppLayout from 'containers/AppLayout';
import './App.scss';

const App: React.FC = () => {
  return (
    <Router>
      <Switch>
        <Route path="/" component={AppLayout} />
      </Switch>
    </Router>
  );
};

export default App;
