import './styles.css';
import React from 'react';
import { Switch, Link, Route } from 'react-router-dom';
import HiddenBug from 'components/DogDetail/1-hidden-bug';
import RevealedBug from 'components/DogDetail/2-revealed-bug';
import FixedVersion from 'components/DogDetail/3-fixed-version';
import { dogServices } from 'services';

function DogList() {
  const [dogs, setDogs]: any = React.useState(null);
  React.useEffect(() => {
    dogServices.getDogs().then(d => setDogs(d));
  }, []);

  if (!dogs) {
    return null;
  }
  return (
    <div>
      <h2>Pick a dog</h2>
      <ul>
        {dogs.map((d: any) => (
          <li key={d.id}>
            <Link to={`/exhaustive-deps/dogs/${d.id}`}>{d.name}</Link>
          </li>
        ))}
      </ul>
    </div>
  );
}

const Apps = [HiddenBug, RevealedBug, FixedVersion];

function ExhaustiveDemo() {
  const [selection, setSelection] = React.useState(0);
  const DogInfo = Apps[selection];

  const renderNavLink = (index: number, content: any) => (
    <li>
      <Link
        className={`nav-link ${selection === index ? 'active' : ''}`}
        onClick={() => setSelection(index)}
        to="/exhaustive-deps/dogs"
      >
        {content}
      </Link>
    </li>
  );
  return (
    <div style={{ height: 700 }} className="exhaustive-demo">
      <h1 style={{ textAlign: 'center' }}>Dogs and Dependencies</h1>
      <ul style={{ listStyle: 'none', padding: 0 }}>
        {renderNavLink(0, 'Hidden Bug')}
        {renderNavLink(1, 'Revealed Bug')}
        {renderNavLink(2, 'FixedVersion')}
      </ul>
      <hr />
      {DogInfo ? (
        <Switch>
          <Route exact path="/exhaustive-deps/dogs" component={DogList} />
          <Route
            exact
            path="/exhaustive-deps/dogs/:dogId"
            component={DogInfo}
          />
        </Switch>
      ) : (
        'Choose a version'
      )}
    </div>
  );
}

export default ExhaustiveDemo;
