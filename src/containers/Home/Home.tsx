import React from 'react';
import Title from 'components/shared/Title';

const Home: React.FC = () => {
  return (
    <div className="home">
      <Title title="Home Page" />
    </div>
  );
};

export default Home;
