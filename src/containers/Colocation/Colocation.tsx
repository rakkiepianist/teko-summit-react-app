import React from 'react';
import Title from 'components/shared/Title';

function sleep(time: number) {
  const done = Date.now() + time;
  while (done > Date.now()) {
    // sleep...
  }
}
// imagine that this slow component is actually slow because it's rendering a
// lot of data (for example).
function SlowComponent({ time, onChange }: any) {
  sleep(time);
  return (
    <div>
      Wow, that was{' '}
      <input
        value={time}
        type="number"
        onChange={e => onChange(Number(e.target.value))}
      />
      ms slow
    </div>
  );
}

function DogName({ time, dog, onChange }: any) {
  return (
    <div>
      <label htmlFor="dog">Dog Name</label>
      <br />
      <input id="dog" value={dog} onChange={e => onChange(e.target.value)} />
      <p>{dog ? `${dog}'s favorite number is ${time}.` : 'enter a dog name'}</p>
    </div>
  );
}

function Colocation() {
  // this is "global state"
  const [dog, setDog] = React.useState('');
  const [time, setTime] = React.useState(200);
  return (
    <>
      <Title title="Colocation - Demo" />
      <br />
      <hr />
      <br />
      <DogName time={time} dog={dog} onChange={setDog} />
      <SlowComponent time={time} onChange={setTime} />
    </>
  );
}

export default Colocation;
