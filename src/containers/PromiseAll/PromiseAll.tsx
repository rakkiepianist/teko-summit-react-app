import React, { useState } from 'react';
import { Bar } from 'react-chartjs-2';
import Title from 'components/shared/Title';
import Button from 'components/shared/Button';
import { productServices } from 'services';
import { requestConstant } from 'constants/index';

interface typeDataSet {
  label: string;
  backgroundColor: string;
  hoverBackgroundColor: string;
  data: Array<number>;
}
interface dataChart {
  labels: Array<string>;
  datasets: typeDataSet[];
}

const PromiseAll: React.FC = () => {
  const numberPage = requestConstant.REQUEST_NUMBER;
  const batch = requestConstant.BATCH;
  const dataInitial: dataChart = {
    labels: [
      'Get product one by one',
      `Get products by batch ${batch}`,
      'Get all products one time',
    ],
    datasets: [
      {
        label: 'Timing (ms)',
        backgroundColor: '#f1be06',
        hoverBackgroundColor: '#f1c320',
        data: [],
      },
    ],
  };

  const [data, setData] = useState(dataInitial);

  const updateTiming = (newdata: Array<number>): dataChart => {
    const oldDataSet: typeDataSet = data.datasets[0];
    let newDataSet = {
      ...oldDataSet,
      data: newdata,
    };
    return {
      ...data,
      datasets: [newDataSet],
    };
  };

  const createNumberPages = () => {
    let page = 1;
    let array = [];
    for (let i = 0; i < numberPage; i += 1) {
      array.push(page);
      page += 1;
    }
    return array;
  };

  const getProductOneByOne = async () => {
    const pages = createNumberPages(); // 100 page, from 1 to 100

    const startTime = Date.now();
    for (let i = 0; i < pages.length; i += 1) {
      await productServices.getProducts(pages[i]);
    }
    const endTime = Date.now();
    let newData: Array<number> = data.datasets[0].data;
    newData[0] = endTime - startTime;
    setData(updateTiming(newData));
  };

  const getProductsByBatch = async (batch: number) => {
    try {
      const pages = createNumberPages();

      const startTime = Date.now();
      for (let i = 0; i < pages.length; i += batch) {
        const pageBatch = pages.slice(i, i + batch);
        let promises: Promise<Object>[] = [];
        pageBatch.map(page => promises.push(productServices.getProducts(page)));
        await Promise.all(
          promises
          // promises.map(p => p.catch(e => ({ e, status: 'rejected' }))) // to filter with status is not rejected
        );
      }
      const endTime = Date.now();
      let newData: Array<number> = data.datasets[0].data;
      newData[1] = endTime - startTime;
      setData(updateTiming(newData));
    } catch (e) {
      alert(e);
    }
  };

  const getAllProductsOneTime = () => {
    let promises: Promise<Object>[] = [];
    const pages = createNumberPages();
    pages.map(page => promises.push(productServices.getProducts(page)));

    const startTime = Date.now();
    Promise.all(
      promises
      // promises.map(p => p.catch(e => ({ e, status: 'rejected' }))) // to filter with status is not rejected
    )
      .then(res => {
        const endTime = Date.now();
        let newData: Array<number> = data.datasets[0].data;
        newData[2] = endTime - startTime;
        setData(updateTiming(newData));
      })
      .catch(error => alert(error));
  };

  return (
    <div className="promise-all">
      <Title title="Promise.all() - Demo" />
      <br />
      <hr />
      <br />
      <div>
        <Button
          text={'Get product one by one'}
          onClick={() => getProductOneByOne()}
          colorBgStyle={{ backgroundColor: '#c34848' }}
        />
        <Button
          text={`Get products by batch ${batch}`}
          colorBgStyle={{ backgroundColor: '#3cd22f' }}
          onClick={() => getProductsByBatch(batch)}
        />
        <Button
          text={'Get all products one time'}
          colorBgStyle={{ backgroundColor: 'black' }}
          onClick={() => getAllProductsOneTime()}
        />
      </div>
      <br />
      <div style={{ width: '50%' }}>
        <Bar data={data} redraw />
      </div>
    </div>
  );
};

export default PromiseAll;
