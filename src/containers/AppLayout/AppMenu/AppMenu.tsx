import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Menu, Icon } from 'antd';

const AppMenu = withRouter(props => {
  const { location } = props;
  const selectedMenu = '/' + location.pathname.split('/')[1];
  return (
    <Menu
      className="app-menu"
      theme="dark"
      mode="inline"
      selectedKeys={[selectedMenu]}
    >
      <Menu.Item key="/">
        <Link to="/">
          <span>
            <Icon type="home" />
            Home
          </span>
        </Link>
      </Menu.Item>
      <Menu.Item key="/exhaustive-deps">
        <Link to="/exhaustive-deps">
          <span>
            <Icon type="cloud-sync" />
            ExhaustiveDepsDemo
          </span>
        </Link>
      </Menu.Item>
      <Menu.Item key="/colocation">
        <Link to="/colocation">
          <span>
            <Icon type="deployment-unit" />
            Colocation
          </span>
        </Link>
      </Menu.Item>
      <Menu.Item key="/promise-all">
        <Link to="/promise-all">
          <span>
            <Icon type="cloud-sync" />
            Promise.all()
          </span>
        </Link>
      </Menu.Item>
    </Menu>
  );
});

export default AppMenu;
