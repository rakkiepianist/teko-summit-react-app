import React from 'react';
import { Layout } from 'antd';

const { Footer } = Layout;

const AppFooter: React.FC = () => {
  return <Footer className="app-footer">Teko Summit ©2019 React Team</Footer>;
};

export default AppFooter;
