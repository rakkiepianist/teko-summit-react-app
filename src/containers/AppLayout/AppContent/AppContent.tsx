import React, { Suspense, lazy } from 'react';
import { Switch, Route } from 'react-router-dom';
import { Layout } from 'antd';

const { Content } = Layout;

const Home = lazy(() => import('containers/Home'));
const Colocation = lazy(() => import('containers/Colocation'));
const PromiseAll = lazy(() => import('containers/PromiseAll'));
const ExhaustiveDepsDemo = lazy(() => import('containers/ExhaustiveDepsDemo'));

const AppContent: React.FC = () => {
  return (
    <Content className="app-content">
      <Suspense fallback={<div>Loading...</div>}>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/colocation" component={Colocation} />
          <Route path="/promise-all" component={PromiseAll} />
          <Route path="/exhaustive-deps" component={ExhaustiveDepsDemo} />
        </Switch>
      </Suspense>
    </Content>
  );
};

export default AppContent;
