import React from 'react';
import { Layout } from 'antd';
import AppHeader from './AppHeader';
import AppMenu from './AppMenu';
import AppContent from './AppContent';
import AppFooter from './AppFooter';
import './AppLayout.scss';

const { Sider } = Layout;

const AppLayout: React.FC = () => {
  return (
    <Layout className="app-layout">
      <AppHeader />
      <Layout>
        <Sider width={200}>
          <AppMenu />
        </Sider>
        <Layout>
          <AppContent />
          <AppFooter />
        </Layout>
      </Layout>
    </Layout>
  );
};

export default AppLayout;
