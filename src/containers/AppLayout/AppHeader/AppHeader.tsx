import React from 'react';
import { Link } from 'react-router-dom';
import { Layout } from 'antd';

const { Header } = Layout;

const AppHeader: React.FC = () => {
  return (
    <Header className="app-header">
      <Link to="/">
        <div className="logo">
          <img
            src="https://static.ybox.vn/2017/10/18/5d65ae6a-b3dc-11e7-8c5e-2e995a9a3302.png"
            alt="logo"
          />
        </div>
      </Link>
    </Header>
  );
};

export default AppHeader;
