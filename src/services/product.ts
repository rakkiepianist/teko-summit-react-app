const getProducts = (page: number) => {
  const requestOptions = {
    method: 'GET',
    // headers: {
    //   Authorization: `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Imlzcy1odHRwczovL2lkLnRla28udm4vIn0.eyJqdGkiOiJablYxRnA0ekkwb29uY20ySlYyNHN5eVYiLCJzdWIiOiI3Nzk1Y2UyZjA1NmQ0N2I4YjhhODNlY2Y4MzcyNjQ5NCIsImlzcyI6Imh0dHBzOi8vaWQudGVrby52bi8iLCJhdWQiOiJiMDYzNjAyYWExMmM0ZjYzYTMxMTQyNDVlMzYzNjMzMyIsImlhdCI6MTU3ODM3MzIyNiwiZXhwIjoxNTc4OTc4MDI2LCJzY29wZSI6Im9wZW5pZCBwcm9maWxlIHJlYWQ6cGVybWlzc2lvbnMifQ.AzexpNPHdEJ4zYStrLV6V9g1GSq-sN6W2eVe4bCKpBKfonO_I306mHVz40qpGu9K5c5XWiICmRBFIWgmdhes0cohp2r3bXj8BNgmyjAbhca01x_mw5boiHbO4Ckv51bccRvxf69RFX8FOJXIKsuyKsab963QLNKBdTh-KcA_d6vIU4jIpyoKh4M4ogpAlpHHaDCYadruOevRDhJ0AR0Ir6uKVtnurKtwdCKizsaF5-CoIO_UtSQSeAyoPr8VP6CfKARUV5Klyk31hIPxqb6LxAAOl888YK4_rItZtXbjYr1TWN0gqJxSD5z4ZBqEv6xnDTgr8r1Nwm5ESuPrmY0F1Q`,
    // },
  };
  return fetch(
    `https://catalog.develop.tekoapis.net/products/?pageSize=10&page=${page}`,
    requestOptions
  ).then(
    response => {
      if (!response.ok) {
        return Promise.reject(response.statusText);
      }
      return response.json();
    },
    error => Promise.reject(error)
  );
};

export default { getProducts };
