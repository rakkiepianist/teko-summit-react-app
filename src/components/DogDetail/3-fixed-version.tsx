import React from 'react';
import { Link } from 'react-router-dom';
import { dogServices } from 'services';

function DogInfo(props: any) {
  const [dog, setDog]: any = React.useState(null);

  const { dogId } = props.match.params;
  React.useEffect(() => {
    dogServices.getDog(dogId).then((d: any) => setDog(d));
  }, [dogId]);

  if (!dog) {
    return null;
  }

  return (
    <div>
      <div>
        <Link to="/exhaustive-deps/dogs">Return to list</Link>
      </div>
      <h2>{dog.name}</h2>
      <img style={{ height: 200 }} alt={dog.name} src={dog.img} />
      <p>{dog.description}</p>
      <div>
        <label htmlFor="temperament">Temperament</label>
        <ul id="temperament">
          {dog.temperament.map((t: any) => (
            <li key={t}>{t}</li>
          ))}
        </ul>
      </div>
      {dog.related.length ? (
        <div>
          <label htmlFor="related">Related Dogs</label>
          <ul id="related">
            {dog.related.map((r: any) => (
              <li key={r.id}>
                <Link to={`/exhaustive-deps/dogs/${r.id}`}>{r.name}</Link>
              </li>
            ))}
          </ul>
        </div>
      ) : null}
    </div>
  );
}

export default DogInfo;
