import React from 'react';

import './Button.scss';

interface ButtonProps {
  text: string;
  onClick: () => void;
  colorBgStyle: Object;
}

const Button: React.FC<ButtonProps> = props => {
  const { text, onClick, colorBgStyle } = props;
  return (
    <button
      type="button"
      className="style-btn"
      style={colorBgStyle}
      onClick={onClick}
    >
      {text}
    </button>
  );
};

export default Button;
