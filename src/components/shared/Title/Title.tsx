import React from 'react';
import './Title.scss';

interface TitleProps {
  title: string;
}

const Title: React.FC<TitleProps> = props => {
  const { title } = props;
  return <div className="title">{title}</div>;
};

export default Title;
