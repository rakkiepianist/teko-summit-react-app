import React from 'react';
import { storiesOf } from '@storybook/react';

import Title from './Title';

storiesOf('Title', module).add('Default', () => (
  <Title title="This is a dummy title" />
));
