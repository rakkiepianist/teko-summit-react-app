import React from 'react';
import Title from './Title';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const wrapper = renderer
    .create(<Title title="This is a dummy title" />)
    .toJSON();
  expect(wrapper).toMatchSnapshot();
});
